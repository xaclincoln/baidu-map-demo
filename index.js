
window.onload = function(){
    // 百度地图API功能
	// 创建Map实例
    var map = new BMap.Map("container");    
    // 初始化地图,设置中心点坐标和地图级别
	map.centerAndZoom(new BMap.Point(119.95, 31.79), 11);  
    //添加地图类型控件
	map.addControl(new BMap.MapTypeControl());   
    // 设置地图显示的城市 此项是必须设置的
	map.setCurrentCity("常州");          
    //开启鼠标滚轮缩放
	map.enableScrollWheelZoom(true);     
}